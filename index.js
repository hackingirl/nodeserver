'use strict' ;

const url = require('url');
const express = require('express');
const app = express();
const PORT = process.env.PORT || 5200;

const capitalizeFirstLetter = ([ first, ...rest ]) =>{
  return first.toLocaleUpperCase() + rest.join('');
} 
const IsJustLetters = (word) => {
  let alpha = true;
  for (let i = 0; i < word.length; i++) {
    alpha = alpha && isNaN(word.charAt(i));
  }
  return alpha;
}

//API 1. 
app.get('/api/capitalize/:name/:surname?', (req, res) => {
  let error = '';
  let name = '';
  const optional = req.params.surname;
  if (req.params.name){
    name = capitalizeFirstLetter(req.params.name);
    if (!IsJustLetters(name)) {
      error = '<h2>Please enter a valid name.</h2> ';
    }
  }
  if (optional){
    const surname = capitalizeFirstLetter(optional);
    if (!IsJustLetters(surname)){
      error += '<h2>Please enter a valid surname, or just delete the surname</h2>';
    }
    if (!error){
      res.json( {'name' : name , 'surname' : surname} );
    }
  }else {
    if (!error){
      res.json( {'name' : name } );
    }
  }
  if (error){
    res.status(400).send(error);
  }
});

//API 1. with parameters
app.get('/api/capitalize', (req, res) => {
  let name = '';
  let surname = '';
  const query = url.parse(req.url,true).query;
  let error = '';
  if (!(query.name || query.surname)){
    res.sendFile(__dirname + '/index.html');
  }else{
    if (query.name){
      name = capitalizeFirstLetter(query.name);
      if (!IsJustLetters(name)) {
        error = '<h2>Please enter a valid name.</h2> ';
      }
    }
    if (query.surname){
      surname = capitalizeFirstLetter(query.surname);
      if (!IsJustLetters(surname)){
        error += '<h2>Please enter a valid surname, or just delete the surname</h2>';
      }
      if (!error){
        res.json( {'name' : name , 'surname' : surname} );
      }
    }else {
      if (!error){
        res.json( {'name' : name } );
      }
    }
    if (error){
      res.status(400).send(error);
    }
  }
});

//API 2.
app.get('/api/sum', (req, res) => {
  const array = req.query.array;
  if (array){
    if (typeof array === 'object'){
      const sum = array.map(i=>Number(i)).reduce((a, b) => {
        return Number.parseInt( a ) + Number.parseInt( b );
      }, 0);
      if (isNaN(sum)){
        res.status(400).send('Please enter an array containing only numbers');
      }else{
        res.status(200).json({ 'result' : sum });
      }
    }else{
      if (isNaN(array)){
        res.status(400).send('Please enter an array containing only numbers');
      }else{
        res.status(200).json({'result' : array});
      }
    }
  }else{
    res.status(400).send('<h1>Please provide an array of parameters </h1>');
  }
});

//API 3.
app.get('/api/unique', (req, res) => {
  const unique = [...new Set(req.query.array)];
  res.json({ 'result' : unique });
})

app.use(function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.listen(PORT, console.log(`Server started at port...${PORT}`));